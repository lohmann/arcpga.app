---
title: "arcpga-app"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{arcpga-app}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>"
)
```

```{r setup}
library(arcpga.app)
```
