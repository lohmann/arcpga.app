#::::::::::::::
#Dockerfile_base
#::::::::::::::
FROM rocker/shiny-verse:latest

RUN apt-get update && apt-get upgrade -y

# Install Git
RUN apt-get update && apt-get install -y --no-install-recommends \
    git-core \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

RUN R -e 'BiocManager::install(c("remotes", "ComplexHeatmap"), ask = FALSE, update = FALSE)' 
RUN R -e 'remotes::install_github("wjawaid/enrichR")'
RUN R -e 'remotes::install_gitlab("lohmann/arcpga.data", host = "https://gitcrcm.marseille.inserm.fr/", auth_token = "***", upgrade = "never")'
RUN R -e 'remotes::install_gitlab("lohmann/arcpga.app", host = "https://gitcrcm.marseille.inserm.fr/", upgrade = "never")'

EXPOSE 3838

CMD R -e "options('shiny.port'=3838,shiny.host='0.0.0.0');library(arcpga.data);library(arcpga.app);arcpga.app::run_app()"


