
<!-- README.md is generated from README.Rmd. Please edit that file -->

# arcpga.app

<!-- badges: start -->

[![Lifecycle:
experimental](https://img.shields.io/badge/lifecycle-experimental-orange.svg)](https://lifecycle.r-lib.org/articles/stages.html#experimental)
<!-- badges: end -->

The goal of arcpga.app is an application that aims to facilitate
the sharing of results, avoid modifications of documents resulting from
bioinformatics analysis and visualise the data from the MetSChem in PDAC
project. This application is complementary to a database related to the
project. It is part of the MetSChem in PDAC project, a project financed
by the ARC Foundation that include the CRCM CiBi and DISC platforms. The
application could be reused for future data exploration or for the
follow-up of our work.

The app will be available on the CRCM DISCShiny web server at
<https://disc.marseille.inserm.fr/discshiny/>

## Installation

You can install the development version of arcpga.app available on
gitlab at:

<https://gitcrcm.marseille.inserm.fr/lohmann/arcpga.app>

The shiny app highly depends on the dataset package arcpga.data
available at:

<https://gitcrcm.marseille.inserm.fr/lohmann/arcpga.data>

``` r
# FILL THIS IN! HOW CAN PEOPLE INSTALL YOUR DEV PACKAGE?
```

## Example

This is a basic example which shows you how to solve a common problem:

``` r
library(arcpga.data)
library(arcpga.app)

## basic example code
```
