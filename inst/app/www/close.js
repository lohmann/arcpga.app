 function initializeCloseButton() {
      var header = $('.navbar > .container-fluid');
      header.append('<div style="float:right; padding-top: 2px"><button id="power_off" type="button"><i class="fas fa-power-off"></i></button></div>');

      // Add click event listener for the close button
      $(document).on('click', '#power_off', function() {
        Shiny.onInputChange('close_app', Math.random());
      });
    }

    // Call the function when the document is ready
    $(document).ready(function() {
      initializeCloseButton();
    });

    Shiny.addCustomMessageHandler('closeWindow', function(message) {
      window.close();
    });