#' Generate_Analysis
#'
#' @description A function to generate analysis based on the provided list of genes and pathways.
#' @param geneList A list of genes.
#' @param databases A vector of databases for enrichment analysis.
#' @param analysisData The dataframe containing analysis results.
#' @param geneColumn The column name in `analysisData` containing gene names.
#' @param pValueColumn (Optional) The column name for p-values in `analysisData`.
#' @param foldChangeColumn (Optional) The column name for fold changes in `analysisData`.
#' @param filterType The type of filter to apply ("auto", "pval", "padj", "none").
#' @import dplyr
#' @importFrom enrichR enrichr listEnrichrDbs
#' @return A list containing the analysis results.
#'
#' @noRd
Generate_Analysis <- function(geneList, databases, analysisData, geneColumn, pValueColumn = NULL, foldChangeColumn = NULL, filterType = "auto") {
  resultTables <- list() # List to store output tables

  pathwayTable <- enrichr(geneList, databases)[[1]]

  if (nrow(pathwayTable) > 0) {
    completeTable <- pathwayTable[, -c(5, 6)]

    if (filterType == "auto") {
      if (sum(completeTable$Adjusted.P.value < 0.05, na.rm = TRUE) > 0) {
        filteredTable <- completeTable[completeTable$Adjusted.P.value < 0.05, ]
        filterNote <- "P.Valeur.Ajustee"
      } else if (sum(completeTable$P.value < 0.05, na.rm = TRUE) > 0) {
        filteredTable <- completeTable[completeTable$P.value < 0.05, ]
        filterNote <- "P.Valeur.Non.Ajustee"
      } else {
        filteredTable <- completeTable
        filterNote <- "Sans Filtre P.Valeur"
      }
    } else if (filterType == "pval") {
      validate(need(sum(completeTable$P.value < 0.05, na.rm = TRUE) > 0, "Pas de resultats"))
      filteredTable <- completeTable[completeTable$P.value < 0.05, ]
      filterNote <- "P.Valeur.Non.Ajustee"
    } else if (filterType == "padj") {
      validate(need(sum(completeTable$Adjusted.P.value < 0.05, na.rm = TRUE) > 0, "Pas de resultats"))
      filteredTable <- completeTable[completeTable$Adjusted.P.value < 0.05, ]
      filterNote <- "P.Valeur.Ajustee"
    } else if (filterType == "none") {
      filteredTable <- completeTable
      filterNote <- "Sans Filtre P.Valeur"
    }

    resultTables$`Base Table` <- completeTable
    resultTables$Comment <- filterNote
    resultTables$`Gene List` <- geneList

    splitGenes <- lapply(strsplit(filteredTable$Genes, ";"), `[`)
    names(splitGenes) <- filteredTable$Term

    uniqueGenes <- splitGenes[!duplicated(splitGenes)]
    geneUnion <- f.union(uniqueGenes)
    pathwaysOfInterest <- uniqueGenes[geneUnion]

    enrichedGenes <- unique(unlist(pathwaysOfInterest))
    resultTables$`Gene Enriched` <- enrichedGenes

    filteredPathwayTable <- filteredTable[filteredTable$Term %in% names(pathwaysOfInterest), ]

    if (!is.null(pValueColumn) & !is.null(foldChangeColumn)) {
      geneFoldChange <- analysisData[analysisData[[geneColumn]] %in% enrichedGenes, c(geneColumn, pValueColumn, foldChangeColumn)]
    } else {
      geneFoldChange <- analysisData[analysisData[[geneColumn]] %in% enrichedGenes, c(geneColumn)]
    }

    lenAsked <- length(geneList)
    lenMatched <- nrow(geneFoldChange)
    lenTotal <- nrow(analysisData)

    foldChangeTable <- generer_FC_table(filteredPathwayTable, geneFoldChange, geneColumn, foldChangeColumn, sublist = pathwaysOfInterest)

    resultTables$FCcheck <- foldChangeTable

    if (filterNote %in% c("P.Valeur.Non.Ajustee", "Sans Filtre P.Valeur")) {
      mergedTable <- merge(foldChangeTable, filteredTable[, c("Term", "P.value")], by.x = "GO", by.y = "Term")
      colnames(mergedTable)[colnames(mergedTable) == "P.value"] <- "pval"
    } else if (filterNote == "P.Valeur.Ajustee") {
      mergedTable <- merge(foldChangeTable, filteredTable[, c("Term", "Adjusted.P.value")], by.x = "GO", by.y = "Term")
      colnames(mergedTable)[colnames(mergedTable) == "Adjusted.P.value"] <- "pval"
    }

    if (!is.null(pValueColumn)) {
      sortedTable <- mergedTable %>% arrange(pval)
    } else {
      sortedTable <- mergedTable
    }

    annotation <- paste(lenMatched, "/", lenAsked)

    resultTables$Table <- filteredPathwayTable
    resultTables$plot1 <- sortedTable

    groupedTable <- sortedTable %>%
      group_by(GO) %>%
      arrange(GO, desc(sens)) %>%
      mutate(lab_ypos = cumsum(length) - 0.5 * length)

    groupedTable$percentage <- sapply(groupedTable$Overlap, function(x) eval(parse(text = x))) * 100
    groupedTable <- groupedTable %>%
      mutate(lab_ypos2 = cumsum(percentage) - 0.5 * percentage)
    resultTables$plot2 <- groupedTable

    return(resultTables)
  } else {
    return(resultTables)
  }
}


#' f.union
#'
#' @description A utils function
#' @param x x
#'
#' @return The return value, if any, from executing the utility.
#' @importFrom matrixStats colMaxs
#' @noRd
f.union <- function(x) {
  n <- length(x)
  m <- matrix(F, nrow = n, ncol = n)
  for (i in 1:n) {
    for (j in 1:n) {
      if (all(x[[j]] %in% x[[i]])) {
        m[i, j] <- max(length(x[[j]]), length(x[[i]]))
      }
    }
  }

  colnames(m) <- names(x)
  rownames(m) <- names(x)

  Keep <- names(which(colMaxs(m) == diag(m)))

  return(Keep)
}



#' generer_FC_table
#'
#' @description A fct function
#' @param Table Table
#' @param OutFOLD OutFOLD
#' @param GeneNameID GeneNameID
#' @param FC FC
#' @param sublist sublist is Pathways_Of_Interest
#'
#' @return The return value, if any, from executing the function.
#'
#' @noRd

generer_FC_table <- function(Table, OutFOLD, GeneNameID, FC, sublist) { # sublist is Pathways_Of_Interest ?
  overlaps <- Table[, c("Term", "Overlap")]
  Data <- data.frame(
    "GO" = character(0), "length" = character(0), "up" = character(0), "down" = character(0), "sens" = character(0),
    "Overlap" = character(0)
  )
  for (el in names(sublist)) {
    go <- strsplit(el, "/")[[1]][1]

    if (!is.null(FC)) {
      vec <- OutFOLD[OutFOLD[[GeneNameID]] %in% sublist[[el]], ][[FC]]
    } else {
      vec <- rep(0, length(sublist[[el]]))
    }
    # if(is.null(FC)){
    #   vec<-rep(0,length(sublist[[el]]))
    # }

    positifs <- vec[vec > 0]
    negatifs <- vec[vec < 0]

    min <- min(vec)
    max <- max(vec)
    sens <- as.factor(sign(max))
    if (sign(min) == sign(max)) {
      len <- length(vec)
      laps <- overlaps[overlaps$Term == el, ]$Overlap
      Data <- rbind(Data, data.frame("GO" = go, "length" = len, "up" = max, "down" = min, "sens" = sens, "Overlap" = laps))
    } else {
      if (!is.null(FC)) {
        lenpos <- length(which(OutFOLD[OutFOLD[[GeneNameID]] %in% sublist[[el]], ][[FC]] > 0))
        lenneg <- length(which(OutFOLD[OutFOLD[[GeneNameID]] %in% sublist[[el]], ][[FC]] < 0))
      } else {
        lenpos <- 0
        lenneg <- 0
      }
      minneg <- min(negatifs)
      minpos <- min(positifs)
      maxneg <- max(negatifs)
      maxpos <- max(positifs)
      lenpath <- strsplit(overlaps[overlaps$Term == el, ]$Overlap, split = "/")[[1]][2]

      Data <- rbind(Data, data.frame("GO" = go, "length" = lenpos, "up" = maxpos, "down" = minpos, "sens" = as.factor(1), "Overlap" = paste0(lenpos, "/", lenpath)))
      Data <- rbind(Data, data.frame("GO" = go, "length" = lenneg, "up" = maxneg, "down" = minneg, "sens" = as.factor(-1), "Overlap" = paste0(lenneg, "/", lenpath)))
    }
  }

  if (!is.null(FC)) {
    return(Data)
  } else {
    return(Data[c(
      "GO",
      "length",
      "Overlap"
    )])
  }
}

#' rewritePlotAndco
#'
#' @description A fct function
#' @param outTables outTables
#' @param bases bases
#' @import ggplot2
#' @importFrom stringr fixed str_replace_all str_split str_wrap
#' @importFrom cowplot plot_grid ggdraw draw_label
#'
#' @return The return value, if any, from executing the function.
#'
#' @noRd

rewritePlotAndco <- function(outTables, bases) { # bases=dbsl
  test3 <- outTables$plot1 # sans FC: "GO"      "length"  "Overlap" "pval"

  len_ask <- length(outTables$`Gene List`)
  len_match <- length(outTables$`Gene Enriched`)
  annot1 <- paste(len_match, "/", len_ask)
  annot2 <- outTables$Comment


  # test3$GO<-factor(test3$GO, levels = unique(test3$GO))
  if ("up" %in% colnames(test3)) {
    a <- ggplot(test3, aes(x = up, xend = down, y = GO, colour = sens)) + #### a ----
      {
        if (nrow(test3) < 20) {
          geom_segment(aes(xend = down, yend = GO), linetype = 1, size = 12)
        } else if (nrow(test3) < 25) {
          geom_segment(aes(xend = down, yend = GO), linetype = 1, size = 10)
        } else {
          geom_segment(aes(xend = down, yend = GO), linetype = 1, size = 4)
        }
      } +
      {
        if (sum(test3$up + test3$down) != 0) {
          if (nrow(test3) < 20) {
            geom_point(data = test3[which(test3$length == 1), ], aes(x = down, y = GO), size = 12)
          } else if (nrow(test3) < 25) {
            geom_point(data = test3[which(test3$length == 1), ], aes(x = down, y = GO), size = 10)
          } else {
            geom_point(data = test3[which(test3$length == 1), ], aes(x = down, y = GO), size = 4)
          }
        }
      } +


      geom_text(aes(x = -Inf, y = Inf, hjust = 0, vjust = 1, label = annot1), size = 10, colour = "black") +
      geom_text(aes(x = -Inf, y = Inf, hjust = 0, vjust = 2, label = annot2), size = 10, colour = "black") +

      scale_x_continuous(name = "FC", limits = c(
        {
          if (min(test3$down) < 0) min(test3$down) - 1 else -2
        },
        {
          if (max(test3$up) > 0) max(test3$up) + 1 else 2
        }
      )) +
      {
        if (nrow(test3) < 20) {
          geom_text(aes(label = str_wrap(paste(GO), width = 40), x = (0) / 2, fontface = 2), size = 9, colour = "black", check_overlap = TRUE)
        } else if (nrow(test3) < 25) {
          geom_text(aes(label = str_wrap(paste(GO), width = 40), x = (0) / 2, fontface = 2), size = 8, colour = "black", check_overlap = TRUE)
        } else {
          geom_text(aes(label = str_wrap(paste(GO), width = 120), x = (0) / 2, fontface = 2), size = 4, colour = "black", check_overlap = TRUE)
        }
      } +
      {
        if (nrow(test3) < 20) {
          geom_text(aes(label = str_wrap(paste(length)), x = (down + up) / 2), size = 7, colour = "white")
        } else if (nrow(test3) < 25) {
          geom_text(aes(label = str_wrap(paste(length)), x = (down + up) / 2), size = 5, colour = "white")
        } else {
          geom_text(aes(label = str_wrap(paste(length)), x = (down + up) / 2), size = 3, colour = "white")
        }
      } +
      ggtitle(paste0("Fold Change for each Pathway")) +
      scale_colour_brewer(palette = "Set1") +
      geom_vline(xintercept = 0, linetype = "dotted") +
      theme_bw() +
      theme(
        plot.title = element_text(hjust = 0.5),
        panel.grid.minor = element_blank(),
        panel.grid.major = element_blank(),
        legend.title = element_blank(),
        legend.position = "none",
        axis.text.y = element_blank(),
        axis.ticks.y = element_blank()
      )
  } else {
    a <- ggplot(test3, aes(y = GO)) +
      theme(axis.ticks.y = element_blank()) +
      theme_bw() +
      theme(
        panel.border = element_blank(),
        panel.grid.major = element_blank()
      )
  }

  df2 <- outTables$plot2
  # df2<-df2%>%arrange(pval)
  # df2$GO<-factor(df2$GO, levels =df2%>%arrange(pval)%>% pull(GO)%>%unique())

  b <- ggplot(data = df2, aes(x = GO, y = percentage)) + #### b ----
    coord_flip() +
    theme_bw() +
    ggtitle("Percentage of Enrichissment") +
    geom_col(aes(fill = sens), width = 0.7) +
    ylab("% Enrichissement") +
    xlab("") +
    {
      if (nrow(test3) < 20) {
        geom_text(aes(y = lab_ypos2, label = length, group = sens), size = 7, color = "black")
      } else if (nrow(test3) < 25) {
        geom_text(aes(y = lab_ypos2, label = length, group = sens), size = 5, color = "black")
      } else {
        geom_text(aes(y = lab_ypos2, label = length, group = sens), size = 3, color = "black")
      }
    } +
    scale_colour_brewer(palette = "Set1") +
    theme(
      plot.title = element_text(hjust = 0.5),
      panel.grid.minor = element_blank(),
      panel.grid.major = element_blank(),
      legend.title = element_blank(),
      legend.position = "none",
      axis.text.y = element_blank(),
      axis.ticks.y = element_blank()
    ) #+

  if ("up" %in% colnames(test3)) {
    plot_row <- plot_grid(a, b, # align = 'hv',
      rel_heights = c(1, 2),
      rel_widths = c(2, 1), labels = "AUTO"
    )
  } else {
    plot_row <- plot_grid(a, b,
      rel_widths = c(1, 3), labels = "AUTO"
    )
  }


  title <- ggdraw() +
    draw_label(gsub("_", " ", bases),
      size = 24,
      fontface = "bold",
      x = 0,
      hjust = 0
    ) +
    theme(
      plot.margin = margin(0, 0, 0, 7)
    )

  figure <- plot_grid(
    title, plot_row,
    ncol = 1,
    rel_heights = c(0.1, 1)
  )
  return(figure)
}
