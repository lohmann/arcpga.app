#' footer UI Function
#'
#' @description A shiny Module.
#'
#' @param id,input,output,session Internal parameters for {shiny}.
#'
#' @noRd
#'
#' @importFrom shiny NS tagList
mod_footer_ui <- function(id) {
  ns <- NS(id)

  tags$footer(
    tags$a(href = "https://cibi.marseille.inserm.fr/", tags$img(height = "50hv", src = "img/logo_cibi_white.png", class = "hover-overlay")),
    tags$a(href = "https://disc.marseille.inserm.fr/", tags$img(height = "50hv", src = "img/tabdisc.png", class = "hover-overlay")),
    tags$a(href = "https://www.fondation-arc.org/", tags$img(height = "50hv", src = "img/whiteARCfundation.png", class = "hover-overlay")),
    tags$a(href = "https://www.inserm.fr/", tags$img(height = "100vh", src = "img/thio-logo-inserm-2x.2.-145x145.png", class = "hover-overlay")),
    tags$a(href = "https://www.crcm-marseille.fr/", tags$img(height = "50hv", src = "img/logo-crcrm-color-2x-173x100.png", class = "hover-overlay")),
    tags$a(href = "https://www.institutpaolicalmettes.fr/", tags$img(height = "50hv", src = "img/footer-logo-ipc-1-e1563282197544.png", class = "hover-overlay")),
    tags$a(href = "https://www.univ-amu.fr/", tags$img(height = "50hv", src = "img/logo-amu.svg", class = "hover-overlay")),
    tags$a(href = "https://www.cnrs.fr/", tags$img(height = "100vh", src = "img/cnrs_white.png", class = "hover-overlay")),
    HTML('
     <!-- Copyright -->
     <div class="text-center p-3 text-dark">
       (C) Copyright 2021-2024
       <a href="https://cibi.marseille.inserm.fr/">CiBi</a>
     </div>
     All Rights Reserved
     <!-- Copyright -->
   '),
    class = "footer_container"
  )
}

#' footer Server Functions
#'
#' @noRd
mod_footer_server <- function(id) {
  moduleServer(id, function(input, output, session) {
    ns <- session$ns
  })
}

## To be copied in the UI
# mod_footer_ui("footer_1")

## To be copied in the server
# mod_footer_server("footer_1")
