#' plot_dim UI Function
#'
#' @description A shiny Module.
#'
#' @param id,input,output,session Internal parameters for {shiny}.
#'
#' @noRd
#' @importFrom datamods i18n
#' @importFrom shiny NS tagList
mod_plot_dim_ui <- function(id, base_value_h = 600, base_value_w = 800) {
  ns <- NS(id)
  tagList(
    fluidRow(
      column(
        width = 12,
        numericInput(ns("plot_height"), paste0(i18n("Plot height"), " (# pixels):"), value = base_value_h),
        numericInput(ns("plot_width"), paste0(i18n("Plot width"), " (# pixels):"), value = base_value_w)
      )
    )
  )
}

#' plot_dim Server Functions
#'
#' @noRd
mod_plot_dim_server <- function(id) {
  moduleServer(id, function(input, output, session) {
    ns <- session$ns
    return(list(
      plot_height = reactive(input$plot_height),
      plot_width = reactive(input$plot_width)
    ))
  })
}

## To be copied in the UI
# mod_plot_dim_ui("plot_dim_1")

## To be copied in the server
# mod_plot_dim_server("plot_dim_1")
