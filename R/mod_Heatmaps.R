#' Heatmaps UI Function
#'
#' @description A shiny Module.
#'
#' @param id,input,output,session Internal parameters for {shiny}.
#'
#' @noRd
#'
#' @importFrom datamods i18n
#' @import ComplexHeatmap
#' @importFrom shiny NS tagList
#' @importFrom shinyWidgets awesomeCheckbox
mod_Heatmaps_ui <- function(id) {
  ns <- NS(id)
  tagList(
    golem_add_external_resources(),
    sidebarPanel(
      width = 3,
      id = "side-panel-mod",
      mod_plot_dim_ui(ns("dimension_heatmaps")),
      selectizeInput(
        inputId = ns("user_metadata_on_heatmap"),
        label = i18n("Patient Metadata on heatmap:"),
        choices = "-",
        selected = "-",
        multiple = TRUE, # allow for multiple inputs
        options = list(create = TRUE)
      ),
      column(width = 6, awesomeCheckbox(ns("clustc"), i18n("Column clustering"), value = TRUE)),
      column(width = 6, awesomeCheckbox(ns("clustr"), i18n("Row clustering"), value = TRUE)),
      mod_add_ggplot_label_ui(ns("annot_complex_heatmap"),init=list("title"=24,"labs"=13.2,"ax"=12,"cand"=10)),
      uiOutput(ns('Brewer_palette_choice'))
    ), # closes sidebarPanel
    mainPanel(
      tabsetPanel(
        id = ns("tabs"),
        tabPanel(
          i18n("Plot"), h3("Heatmap"),
          downloadButton(ns("downloadHeat"), i18n("Download heatmap table")),
          downloadButton(ns("downloadPlotPDF"), i18n("Download pdf-file")),
          downloadButton(ns("downloadPlotPNG"), i18n("Download png-file")),
          div(
            style = "position:relative",
            plotOutput(ns("coolplot_heatmap"),
              height = "auto",
              # click = "clicked",
              # hover = hoverOpts(ns("plot_hover"), # dunno about thisns
              hover = hoverOpts(ns("plot_hover"), # dunno about thisns
                delay = 10, delayType = "debounce"
              )
            ),
            uiOutput(ns("hover_info"))
          )
        )
       # , tabPanel(
       #    i18n("iPlot"), h3("Heatmap")
       #  )
      )
    ) # closes mainPanel
  ) # closes tagList
}

#' Heatmaps Server Functions
#'
#' @importFrom datamods i18n
#' @import ComplexHeatmap
#' @importFrom rlang sym
#' @importFrom circlize colorRamp2
#' @importFrom RColorBrewer brewer.pal
#' @importFrom grDevices colorRampPalette
#' @noRd
mod_Heatmaps_server <- function(id, r_global) {
  moduleServer(id, function(input, output, session) {
    ns <- session$ns
    heatmap_object <- reactiveVal()
    column_desc <- reactiveVal()

    plot_settings <- mod_plot_dim_server("dimension_heatmaps")
    plot_annot <- mod_add_ggplot_label_server("annot_complex_heatmap", r_global)


    r_global$table_to_create_heatmap <- reactive({
      r_global$df_upload() |> filter(!!sym(r_global$g_choice) %in% r_global$names_top())
    })



    observe({
      updateSelectizeInput(session, "user_metadata_on_heatmap", choices = colnames(r_global$metatable_to_visualize))
    })

    width <- reactive({
      plot_settings$plot_width()
    })
    height <- reactive({
      plot_settings$plot_height()
    })


    Heat_table <- reactive({
      req(r_global$table_to_create_heatmap())
      req(r_global$names_top)
      req(r_global$g_choice)

      target <- grep("P[[:digit:]]", colnames(r_global$table_to_create_heatmap()), value = T)

      heat <- extract_heatmap(
        r_global$table_to_create_heatmap(),
        r_global$g_choice,
        target
      )
     
      modified <- scale_mat(heat, input$scaling)
      return(modified)
    })

    Column_Heat <- reactive({
      req(r_global$metatable_to_visualize)
      if (is.null(input$user_metadata_on_heatmap) ||
        length(input$user_metadata_on_heatmap) == 0 ||
        all(input$user_metadata_on_heatmap == "-")) {
        return(NULL)
      }
      metab <- r_global$metatable_to_visualize[, input$user_metadata_on_heatmap, drop = FALSE]

      return(metab)
    })


    output$Brewer_palette_choice <- renderUI({
      fluidRow(
        column(width = 6,
               pickerInput(
                 inputId = ns("selected_heat_color"),
                 label = i18n("Palette color"), 
                 choices = rownames(RColorBrewer::brewer.pal.info %>% filter(colorblind & category == "div")),
                 selected="RdYlBu"
               )
        ),
        column(width = 6,
               pickerInput(
                 inputId = ns("scaling"),
                 label = i18n("Scaling"), 
                 choices = c("none", "row", "column"),
                 selected="none"
               )
               )
      )
    })
    output$coolplot_heatmap <- renderPlot(width = width, height = height, {
      req(Heat_table())


      heat_name <- if (plot_annot$add_title()) {
        paste(plot_annot$title(), "\n", sep = "")
      } else {
        NULL
      }
      
      col_name <- if (plot_annot$label_axes()) {
        paste(plot_annot$lab_x(), "\n", sep = "")
      } else {
        NULL
      }
      
      row_name <-  if (plot_annot$label_axes()) {
        paste(plot_annot$lab_y(), "\n", sep = "")
      } else {
        NULL
      }


      if (!is.null(Column_Heat())) {
        row_heatmap <- Column_Heat() %>%
          dplyr::mutate(
            across(
              everything(),
              ~ if (is.numeric(.x)) {
                as.factor(.x)
              } else {
                droplevels(factor(.x))
              }
            )
          )

                column_desc(row_heatmap)


        row_ <- HeatmapAnnotation(
          df = row_heatmap,
          col = merged_color_list,
          gp = grid::gpar(col = "white")
        )
      } else {
        row_ <- NULL
        column_desc(NULL)
      }

      print(Heat_table())


      # Create the heatmap object
      heatmap <- ComplexHeatmap::Heatmap(
        Heat_table(),
        cluster_columns = input$clustc,
        row_title = row_name,
        row_title_gp = grid::gpar(fontsize = plot_annot$fnt_sz_labs()),
        column_title_gp = grid::gpar(fontsize = plot_annot$fnt_sz_labs()), 
        cluster_rows = input$clustr,
        top_annotation = row_,
        rect_gp = grid::gpar(col = "white"),
        name =  switch(input$scaling,
                       "none" = "TMM",
                       "row" =  i18n("Row Scaled Expression"),
                       "column" =  i18n("Column Scaled Expression")),
        heatmap_legend_param = list(
          title_gp = grid::gpar(fontsize = plot_annot$fnt_sz_cand(), fontface = "bold"),
          legend_direction = "horizontal"
          # ,labels_gp =grid::gpar(fontsize = plot_annot$fnt_sz_cand())
        ),
        col = colorRamp2(
          seq(
            stats::quantile( Heat_table(), probs = c(0.025)),
            stats::quantile( Heat_table(), probs = c(0.925)),
            
            l = n <- 9
          ),
          colorRampPalette(rev(brewer.pal(11, input$selected_heat_color)))(n)
        ),
        column_title = col_name, 
        column_names_gp = grid::gpar(fontsize = plot_annot$fnt_sz_ax()), 
        row_names_gp = grid::gpar(fontsize = plot_annot$fnt_sz_ax()),
        column_title_side = "top" 
      )

      # Draw the combined heatmap and annotations
      showing_plot <- ComplexHeatmap::draw(
        heatmap, #+ row_anno + column_anno,
        column_title= heat_name,
        column_title_gp=grid::gpar(fontsize=plot_annot$fnt_sz_title()),
        show_heatmap_legend = plot_annot$add_legend(),
        show_annotation_legend = plot_annot$add_legend(),
        heatmap_legend_side = "bottom"
        
        
      )

      # print(column_desc()[,column_order(showing_plot),drop=FALSE])

      heatmap_object(showing_plot)
    })


    output$downloadHeat <- downloadHandler(
      filename = function() {
        paste("Heat_table", Sys.time(), ".gct", sep = "")
      },
      content = function(file) {
        r_o <- row_order(heatmap_object())
        c_o <- column_order(heatmap_object())

        t_heat <- Heat_table()[r_o, c_o]


        if (!is.null(column_desc())) {
          col_desc <- column_desc()[c_o, , drop = FALSE]
        } else {
          col_desc <- NULL
        }


        # Write the GCT file
        write_gct(t_heat,
          # row_metadata = row_meta,
          col_metadata = col_desc,
          file = file
        )
      }
    )
    
    
    
    ######### DEFINE DOWNLOAD BUTTONS FOR ORDINARY PLOT ###########
    
    output$downloadPlotPDF <- downloadHandler(
      filename <- function() {
        paste("Heatmap_arcpga_", Sys.time(), ".pdf", sep = "")
      },
      content <- function(file) {
        # pdf(file, width = input$plot_width / 72, height = input$plot_height / 72)
        pdf(file, width = plot_settings$plot_width() / 72, height = plot_settings$plot_height() / 72)
        plot(heatmap_object())        
        dev.off()
      },
      contentType = "application/pdf" # MIME type of the image
    )
    
    
    output$downloadPlotPNG <- downloadHandler(
      filename <- function() {
        paste("Heatmap_arcpga_", Sys.time(), ".png", sep = "")
      },
      content <- function(file) {
        # png(file, width = input$plot_width * 4, height = input$plot_height * 4, res = 300)
        png(file, width = plot_settings$plot_width() * 4, height = plot_settings$plot_height() * 4, res = 300)
        plot(heatmap_object())
        
        dev.off()
      },
      contentType = "application/png" # MIME type of the image
    )
    
    
    
    
  })
}
