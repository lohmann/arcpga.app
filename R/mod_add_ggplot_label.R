#' add_ggplot_label UI Function
#'
#' @description A shiny Module.
#'
#' @param id,input,output,session Internal parameters for {shiny}.
#' @param init list of initiated values 
#' @noRd
#'
#' @importFrom shiny NS tagList
#' @importFrom datamods i18n
mod_add_ggplot_label_ui <- function(id , init=list("title"=24,"labs"=24,"ax"=18,"cand"=6)) {
  ns <- NS(id)
  tagList(
    h4(i18n("Labels")),
    awesomeCheckbox(
      inputId = ns("add_title"),
      label = i18n("Add title"),
      value = FALSE
    ),
    conditionalPanel(
      condition = "input.add_title == true",
      textInput(ns("title"), i18n("Title:"), value = ""), ns = ns
    ),
    awesomeCheckbox(
      inputId = ns("label_axes"),
      label = i18n("Change axis labels"),
      value = FALSE
    ),
    conditionalPanel(
      condition = "input.label_axes == true", ns = ns,
      textInput(ns("lab_x"), i18n("X-axis:"), value = ""),
      textInput(ns("lab_y"), i18n("Y-axis:"), value = "")
    ),
    awesomeCheckbox(
      inputId = ns("adj_fnt_sz"),
      label = i18n("Change font size"),
      value = FALSE
    ),
    conditionalPanel(
      condition = "input.adj_fnt_sz == true", ns = ns,
      numericInput(ns("fnt_sz_title"), i18n("Plot title:"), value = init$title),
      numericInput(ns("fnt_sz_labs"), i18n("Axis titles:"), value = init$labs),
      numericInput(ns("fnt_sz_ax"), i18n("Axis labels:"), value = init$ax),
      numericInput(ns("fnt_sz_cand"), i18n("Labels of hits:"), value = init$cand)
    ),
    awesomeCheckbox(
      inputId = ns("add_legend"),
      label = i18n("Add legend"),
      value = FALSE
    )
  )
}

#' add_ggplot_label Server Functions
#'
#' @noRd
mod_add_ggplot_label_server <- function(id, r_global) {
  moduleServer(id, function(input, output, session) {
    ns <- session$ns


    return(list(
      add_title = reactive(input$add_title),
      title = reactive(input$title),
      label_axes = reactive(input$label_axes),
      lab_x = reactive(input$lab_x),
      lab_y = reactive(input$lab_y),
      adj_fnt_sz = reactive(input$adj_fnt_sz),
      fnt_sz_title = reactive(input$fnt_sz_title),
      fnt_sz_labs = reactive(input$fnt_sz_labs),
      fnt_sz_ax = reactive(input$fnt_sz_ax),
      fnt_sz_cand = reactive(input$fnt_sz_cand),
      add_legend = reactive(input$add_legend)
    ))
  })
}

## To be copied in the UI
# mod_add_ggplot_label_ui("add_ggplot_label_1")

## To be copied in the server
# mod_add_ggplot_label_server("add_ggplot_label_1")
