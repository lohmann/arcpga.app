#' dataset_filter UI Function
#'
#' @description A shiny Module.
#'
#' @param id,input,output,session Internal parameters for {shiny}.
#'
#' @noRd
#' @importFrom datamods filter_data_ui
#' @importFrom shiny NS tagList
mod_dataset_filter_ui <- function(id) {
  ns <- NS(id)
  tagList(
    # div(mod_integrated_filters_ui(ns("own_filters")), style = "overflow-y: auto; overflow-x: hidden;  max-height:500px;" ),

    mod_integrated_filters_ui(ns("own_filters")),
    conditionalPanel(
      condition = "output.LoadedTable",
      column(width = 12, filter_data_ui(ns("filtering"), max_height = "500px"))
    ),
    conditionalPanel(
      condition = "output.LoadedTable",
      column(
        width = 12,
        textInput(ns("fc_cutoff"), paste0(i18n("Fold Change threshold"), " (min,max):"), value = "0,0")
      )
    ), mod_add_n_remove_widgets_ui(ns("gene_name_sel")),
    uiOutput(ns("create_an_annotation")),
    uiOutput(ns("alert_filtering")),
    tags$br(),
    uiOutput(ns("launch_explore_subset"))
  )
}

#' dataset_filter Server Functions
#' @importFrom datamods filter_data_server i18n
#' @noRd
mod_dataset_filter_server <- function(id, r_global) {
  moduleServer(id, function(input, output, session) {
    ns <- session$ns


    mod_integrated_filters_server("own_filters", r_global)


    mod_add_n_remove_widgets_server("gene_name_sel", r_global) # contains pickerinput r_global$to_add


    ref <- reactive({
      intersect(colnames(r_global$loaded_table), colnames(GeneIDS))
    })


    # file <- reactive({
    #   if (!is.null(r_global$integrated_filter)) {
    #     merge(r_global$loaded_table, r_global$integrated_filter[, ref(), drop = FALSE], by = ref(), sort = FALSE) |> unique()
    #   } else {
    #     r_global$loaded_table
    #   }
    # })


    file <- reactive({
      # Create a copy of the loaded_table
      modified_table <- r_global$loaded_table

      # Check if r_global$to_add is not null and has values
      if (!is.null(r_global$to_add) & !is.null(ref())) {
        modified_table <- merge(GeneIDS[c(ref(), r_global$to_add)], modified_table, by = ref(), all.y = TRUE) |> unique()
      }

      fc_cutoff <- strsplit(input$fc_cutoff, ",")[[1]]

      # Check if fc_cutoff is a valid input
      if (length(fc_cutoff) == 2) {
        foldchange_min <- as.numeric(fc_cutoff[1])
        foldchange_max <- as.numeric(fc_cutoff[2])

        # Apply filter if fc_cutoff is different from "0,0"
        if (!(foldchange_min == 0 & foldchange_max == 0) && !is.na(foldchange_min) && !is.na(foldchange_max)) {
          if ("logFC" %in% colnames(modified_table)) {
            modified_table <- modified_table %>%
              filter(logFC < foldchange_min | logFC > foldchange_max)
          }
        }
      }

      if (!is.null(r_global$integrated_filter)) {
        modified_table <- merge(modified_table, r_global$integrated_filter[, ref(), drop = FALSE], by = ref(), sort = FALSE) |> unique()
      }

      modified_table
    })


    r_global$res_filter <- filter_data_server(
      id = "filtering",
      data = file,
      widget_num = "range",
      widget_char = "picker"
    )


    rows_similar <- reactive({
      req(r_global$loaded_table)
      nrow(r_global$loaded_table) == nrow(r_global$res_filter$filtered())
    })

    # observe({
    #   print(r_global$res_filter$filtered())
    # })

    output$create_an_annotation <- renderUI({
      req(r_global$loaded_table)

      fluidPage(
        textInput(ns("personalized_annotation"), i18n("Note:"), ""),
        actionButton(ns("annotate_the_table_with_filter"), i18n("Create a filter annotation"))
      )
    })


    output$launch_explore_subset <- renderUI({
      req(r_global$loaded_table)
      if (rows_similar()) {
        tagList(
          column(
            width = 12,
            HTML(paste0('
      <div class="container-custom mt-3">
      <div class="alert alert-warning">
      <strong>', i18n("This table has not been filtered and may slow down further analysis."), '</strong>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">x</button>
      </div>
                       </div>')),
            div(
              align = "center",
              actionBttn(ns("launch_the_vizdata"), i18n("Visualize this table"),
                style = "material-flat",
                color = "primary"
              )
            )
          )
        )
      } else {
        column(width = 12, div(
          align = "center",
          actionBttn(ns("launch_the_vizdata"), i18n("Visualize this table"),
            style = "material-flat",
            color = "primary"
          )
        ))
      }
    })

    # Reactive value to store the flag indicating if annotation is needed
    should_annotate <- reactiveVal(FALSE)

    observeEvent(input$annotate_the_table_with_filter, {
      should_annotate(TRUE)
      ref <- intersect(colnames(r_global$res_filter$filtered()), colnames(GeneIDS))

      data <- r_global$res_filter$filtered()
      data$annotation <- input$personalized_annotation # Use the personalized annotation input
      r_global$annotated_data <- data[, c(ref, "annotation")]
    })



    toviztab <- eventReactive(input$launch_the_vizdata, {
      ref <- intersect(colnames(r_global$res_filter$filtered()), colnames(GeneIDS))

      data <- r_global$res_filter$filtered()

      columns_to_check <- c(
        "P.Value" = "minus_log10_pvalue",
        "PValue" = "minus_log10_pvalue",
        "adj.P.Val" = "minus_log10_adj_pvalue",
        "FDR" = "minus_log10_adj_pvalue"
      )

      existing_columns <- columns_to_check[names(columns_to_check) %in% colnames(data)]

      data <- data %>%
        mutate(across(all_of(names(existing_columns)),
          .fns = ~ -log(as.numeric(.)),
          .names = "{existing_columns[.col]}"
        ))

      # Conditionally merge annotation column if annotation flag is set
      if (should_annotate()) {
        annotation_data <- r_global$annotated_data
        ref_common <- intersect(colnames(data), colnames(annotation_data))
        data <- merge(data, annotation_data, by = ref_common, all.x = TRUE)
      }


      merged_data <- merge(data, r_global$linked_tmm, all.x = TRUE, sort = FALSE)
      return(merged_data)
    })


    tovizmetab <- eventReactive(input$launch_the_vizdata, {
      r_global$loaded_metatable
    })

    observe({
      r_global$table_to_visualize <- toviztab()
    })
    observe({
      r_global$metatable_to_visualize <- tovizmetab()
    })


    # observe({
    #   print(annotation_column())
    #   })
  })
}
