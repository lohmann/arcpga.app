library(shiny)
library(tidyverse)

ui <- fluidPage(
  sidebarLayout(
    sidebarPanel(fluidRow(
      column(4, selectInput("COLUMN", "Filter By:", choices = colnames(iris))),
      column(4, selectInput("CONDITION", "Boolean", choices = c("==", "!=", ">", "<"))),
      column(4, uiOutput("COL_VALUE"))
    )),
    mainPanel(tableOutput("the_data"))
  )
)

server <- function(input, output) {
  output$COL_VALUE <- renderUI({
    x <- iris %>% select(!!sym(input$COLUMN))
    selectInput("VALUE", "Value", choices = x)
  })

  filtering_string <- reactive({
    paste0("!!sym(", input$COLUMN, ") ", input$CONDITION, " ", input$VALUE)
  })

  output$the_data <- renderTable({
    if (input$VALUE == "") {
      my_data <- ""
    } else {
      my_data <- iris %>%
        filter(eval(parse(text = paste0(input$COLUMN, input$CONDITION, input$VALUE))))
    }

    return(my_data)
  })
}

shinyApp(ui = ui, server = server)
